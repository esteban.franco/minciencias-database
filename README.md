# Minciencias DataBase

Se proyecta generar una librería para escarpear los gruplac de los grupos de investigación que se encuentran catalogados en la plataforma Minciencias y devolverlos en bases de datos estructuradas, asi como los CVlacs de los investigadores. 

La intención es que los líderes de investigación y los directores de investigación de las Universidades puedan usar esta librería para el estudio de sus grupos de investigación y de sus investigadores por medio del análisis de datos.

Minciencias.py: Archivo con las clases y métodos necesarios para la extracción de información del gruplac y cvlac del investigador con base a su respectivo url.

**extraccion_cvlac.py:** script ejemplo para descargar la información de cada investigador usando la librería minciencias.py

**estructura_CVLAC.xlsx:** Estructura de organización de la información que se puede recolectar de un investigador. 

**estructura_gruplac.xlsx:** Estructura de organización de la información que se puede recolectar de un grupo de investigación.

**Visualizacion_CVlac.ipynb:** Notebook de bosquejo para diseñar cada método de la clase Investigador.

**Visualización_GrupLac.ipynb:** Notebook de bosquejo para diseñar cada método de la clase Grupo. 