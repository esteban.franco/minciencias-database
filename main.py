# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 16:14:55 2020
Este programa extraera toda la informacíon de un investigador encontrada en su cvlac público
y la almacenará en un dicciónario.

@author: Esteban Franco
"""
import requests
import bs4  #importando beautiful soup
import pandas as pd
import pickle
from time import sleep
import re
from minciencias import Investigador, Grupo
#%% VARIABLES PRINCIPALES

group_path = (r'C:\Users\Estef\OneDrive - Universidad de San Buenaventura Medellín'
              ' (1)\coordinacion-invesigaciones-facultad\codigos\Documentación-de-grupos')
group = 'GIMSC'

df_group = pd.read_excel('{}/{}/INTEGRANTES ACTIVOS p.xlsx'.format(group_path,group))

CVLAC ={}
#%% Analizando el grupo
gruplac_url = 'https://scienti.minciencias.gov.co/gruplac/jsp/visualiza/visualizagr.jsp?nro=00000000002587'
grupo = Grupo()


# grupo.lecturaGroupLac(gruplac_url)
# grupo.basicos()
# grupo.capitulos_libro()
# grupo.libros()
# grupo.articulos()
# grupo.eventos()
# grupo.proyectos_investigacion()
# print('En espera 10 segundos...')
# sleep(10)


#%% Analizando miembros del grupo
N = 4

miembro = Investigador(df_group.loc[N,'NOMBRES'], str(df_group.loc[N,'IDENTIFICACION']))
miembro.lecturaCVLac(df_group.loc[N,'CVLAC'])

# %% Recolectando datos del investigador
if miembro.response:
    miembro.identificacion()
    miembro.form_academica()
    miembro.form_complementaria()
    miembro.experiencia_profesional()
    miembro.lineas_investigacion()
    miembro.eventos()
    miembro.Articulos()
    miembro.libros_investigacion()
    miembro.capitulos_de_libro()
# else:
#     print('No se puedo leer el cvlac de {}'.format(miembro.name))