# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 22:08:54 2020

@author: Esteban Franco
"""

class Investigador():

    def __init__(self, name, ID):

        from datetime import datetime
        self.name = name
        self.ID = int(ID) if ID.isdigit() else ID
        self.update = [datetime.now()]
        self.cvlac_url = None
        self.cvlac_html = None
        self.fecha_cvlac = None  # fecha en la que se escrapeó el cvlac por última vez
        self.start_date = datetime.now()
        self.datos_generales = {'Identificacion':None, # Done
                                'Formación Académica':None, # Done
                                'Formación complementaria':None, # Done
                                'Experiencia profesional':None, # Done
                                'Áreas de actuación':None,
                                'Idiomas':None,
                                'Lineas de investigación':None, # Done
                                'Reconocimientos':None}
        self.actividades_de_formacion = {'Asesoria Programa Ondas':None,
                                         'Curso de corta duracion':None,
                                         'Trabajos dirigidos/tutorias':None}
        self.actividades_de_evaluador ={'Jurado en comités de evaluación':None,
                                'Par evaluador':None,
                                'Participación en comités de evaluación':None}
        self.apropiacion_social = {'Eventos cientificos':None, # Done
                               'Redes de conocimiento especializado':None,
                               'Generación de contenido impresa':None,
                               'Generación de contenido multimedia':None,
                               'Generación de contenido virtual':None,
                           'Estrategia de comunicación del conocimiento':None,
                           'Estrategia pedagógica para el fomento a la CTI':None,
                           'Espacio de participación ciudadana':None,
                           'Participación ciudadana en proyectos del CTI':None}
        self.produccion_bibliografica = {'Artículo':None,
                                         'Libro':None,
                                         'Capítulo de libro':None}
        self.produccion_tecnica = {'Software':None,
                                   'Productos tecnológicos':None,
                                   'Procesos o técnicas':None,
                                   'Trabajos técnicos':None,
                                   'Normas y regulaciones':None,
                                   'Otra producción técnica':None,
                                   'Empresas de base tecnológica':None}
        self.mas_informacion ={'Demás trabajos':None,
                               'Proyectos':None}



    def lecturaCVLac(self,cvlac_url):
        """
        Metodo para leer el cvlac del investiador basado y guardarlo
        en una soup de Beautifulsoup

        Parameters
        ----------
        cvlac_url : str
            DESCRIPTION.direccción web del cvlac

        Returns
        -------
        self.secciones['Identificación']
        """
        import requests, bs4, time
        from datetime import datetime
        self.cvlac_url = cvlac_url
        try:
            response = requests.get(url= cvlac_url, verify=False)
            print('Leyendo CvLac de {} \n:'.format(self.name))
            cvlac_soup = bs4.BeautifulSoup(response.text,'lxml')
            self.response = response.status_code
            self.cvlac_html = cvlac_soup
            self.fecha_cvlac = datetime.today() # última fecha de web scrapping
            print('Obteniendo las secciones de su CvLac \n')
            tablas = (cvlac_soup.find('div',attrs={'class':'container'}).
                      find_all('table', attrs={'style':'border:#999 1px solid'}))
            # obteniendo un diccionario con todas las secciones tipo tabla del cvlac
            self.secciones = (dict.fromkeys([ tabla.find('h3').
                                             text for tabla in tablas[1:]],'NA'))
            # agregando la tablas[0] que tiene los datos de identificación
            self.secciones['Identificación'] = tablas[0]
            for tabla in tablas[1:]: # loop por toda la informción de las tablas
                for section in self.secciones.keys(): #loop por cada sección
                    if tabla.find('h3'): # si existe un titulo en esta tabla
                        # y el título coíncide con la llave de la sección
                        if section == tabla.find('h3').text:
                            self.secciones[section] = tabla # son tipo bs4.tag

            time.sleep(8)
        except Exception as e:
            print('No se pudo leer cvlac de {}, error {}'.format(self.name
                                                                 ,e))
            self.response = False
        return

    def update_cvlac(self,cvlac_url):
        """ actualizar el escrapeo del CvLac"""
        if (self.update[-1].date() - self.fecha_cvlac.date()).days < 30:
            print('Actualizando scrapper del CvLac de {}:'.fomat(self.name))
            self.lecturaCVLac(cvlac_url)
        else: print('CvLac scrapeado a la última versión')
#%% --------------DATOS GENERALES ---------------------------------------
    def identificacion(self):
        """Agregar un diccionario con todos los datos de identificación
            del investigador tomados de sus cvlac en su campo de
            datos generales"""
        import pandas as pd
        import numpy as np

        if self.response and 'Identificación' in self.secciones:
            print('Agregando datos generales - Identificación')

            general_data = self.secciones['Identificación'].find_all('tr')


            campos = dict.fromkeys(['Par evaluador reconocido por Minciencias',
                               'Categoría','Nombre','Nombre en citaciones',
                               'Nacionalidad','Sexo','Código ORCID',
                               'Author ID SCOPUS'],None)

            initial_info = np.asanyarray(list(campos.values())).reshape(1,8)
            identificacion = pd.DataFrame(data=initial_info, columns=campos.keys())

            for data in general_data:
                dato = [camp.text for camp in data.find_all('td')]

                # Es para evaluador?
                if data.find('td',attrs={'class':'messageText'}):
                    identificacion['Par evaluador reconocido por Minciencias'] = 'si'

                if 'Categoría' in dato[0]: # Categoria de investigador?
                    identificacion['Categoría'] = dato[1].split('(')[1].split(')')[0]

                # Encontrando el nombre
                if dato[0] in 'Nombre': # recopilando el nombre completo
                    complete_name = dato[1].split('\n')[1]
                    nombres = complete_name.split('\xa0')[0].replace('  ','')
                    apellidos = ' '.join(complete_name.split('\xa0')[1:]).replace('  ',' ')
                    identificacion['Nombre'] = nombres+' '+apellidos

                if dato[0] in 'Nombre en citaciones': # Nombre en citaciones
                    identificacion['Nombre en citaciones'] = dato[1]

                if dato[0] in 'Nacionalidad': # Tomando nacionalidad
                    identificacion['Nacionalidad'] = dato[1]

                if dato[0] in 'Sexo': # Tomando sexo
                    identificacion['Sexo'] = dato[1]

                # Encontrando orcid o scopus
                if data.find_all('a', href=True):
                    enlace = data.find('a', href=True)
                    if str(enlace.text.replace('\n','')) in 'Código ORCID':
                        identificacion['Código ORCID'] = enlace['href']
                    if str(enlace.text.replace('\n','')) in 'Author ID SCOPUS':
                        identificacion['Author ID SCOPUS'] = enlace['href']

            self.datos_generales['Identificacion'] = identificacion
        else:
            if not self.response:
                print('Falta lectura del cvlac de {}'.format(self.name))
            else:
                print('Aún no se han leido los datos de identificación'
                  'del CvLac de {}'.format(self.name))
            self.datos_generales['Identificacion'] = None
        return


    def form_academica(self):
        """Agregar una lista de diccionarios con cada nivel de estudios del
        investigador en un campo de datos generales"""

        import pandas as pd

        if (self.response and 'Formación Académica' in self.secciones and
            len(self.secciones['Formación Académica'].find_all('td')) > 2):

            print('Agregando datos generales - Formación académica')

            niveles_form = self.secciones['Formación Académica'].find_all('td')
            niveles_form = niveles_form[2:]

            # campos para llenar un nivel de estudios

            formaciones = []
            for nivel in niveles_form:
                if nivel.find('b'):
                    ejemplo_formacion = Curso_profesional(nivel)
                    formaciones.append(ejemplo_formacion)

            formacion_academica = pd.concat(formaciones,ignore_index=True)
            self.datos_generales['Formación Académica'] = formacion_academica

        else:
            if not self.response:
                print('Falta lectura del cvlac de {}'.format(self.name))
            else:
                print('Aún no se han leido los datos de formación académica'
                  'del CvLac de {} \n'.format(self.name))
            self.datos_generales['Formación Académica'] = None

    def form_complementaria(self):
        """Tomar todas las formaciónes complementarias de un investigador y devolverlas
        en una lista de diccionarios"""
        import pandas as pd


        if (self.response and 'Formación Complementaria' in self.secciones
            and len(self.secciones['Formación Complementaria']) > 1):

            print('Agregando datos generales - formación complementaria')
            cursos = (self.secciones['Formación Complementaria'].
                      find_all('td')[1:]) # sin tomar el título

            # encontrando los tag que son verdaderamente cursos
            curso_pequenho = [curso for curso in cursos if curso.find('b')]

            complementarios = []

            for info_curso in curso_pequenho:
                ejemplo_curso = Curso_complementario(info_curso)
                complementarios.append(ejemplo_curso)

            formacion_complement = pd.concat(complementarios,ignore_index=True)
            self.datos_generales['Formación complementaria'] = formacion_complement
        else:
            if not self.response:
                print('Falta lectura del cvlac de {}'.format(self.name))
            else:
                print('No se encuentran datos de formación complementaria de {} \n'.
                      format(self.name))
            self.datos_generales['Formación complementaria'] = None

    def experiencia_profesional(self):
        """Toma la sección de experiencia laboral del investigador
        y devuelve una lista de diccionario, donde cada diccionario
        es la información relacionada detallada de cada trabajo"""


        if (self.response and 'Experiencia profesional' in self.secciones and
            len(self.secciones['Experiencia profesional'].find_all('td')) > 1):

            print('Agregando datos generales - experiencia profesional')
            campos = self.secciones['Experiencia profesional'].find_all('td')[1:]

            Trabajos = [campo for campo in campos if campo.find('b')]

            experiencias_laborales = []
            for trabajo in Trabajos:
                experiencia_laboral = historial_posicion(trabajo)
                experiencias_laborales.append(experiencia_laboral)


            self.datos_generales['Experiencia profesional'] = experiencias_laborales
        else:
            if not self.response:
                print('Falta lectura del cvlac de {}'.format(self.name))
            else:
                print('Aún no se han leido los datos de experiencia profesional '
                  'del CvLac de {} \n'.format(self.name))
            self.datos_generales['Experiencia profesional'] = None

        return

    def lineas_investigacion(self):
        """
        metodo para retornar un DataFrame con las lineas de investigación del
        investigador

        Parameters
        ----------
        self.secciones['Líneas de investigación']
        TYPE: bs4.tag
        DESCRIPTION Listas con todas las lineas de investigación

        Returns
        -------
        None
        """
        import pandas as pd
        if (self.response and 'Líneas de investigación' in self.secciones and
            len(self.secciones['Líneas de investigación'].find_all('tr')) > 1):

            print('Agregando datos generales - lineas de investigación')
            lineas_invest = self.secciones['Líneas de investigación'].find_all('tr')[1:]

            lineas = pd.DataFrame(columns=['Línea','Activa'])

            for i, linea in enumerate(lineas_invest):

                lineas.loc[i,'Línea'] = linea.find('li').text.split(', Activa')[0].strip('\xa0')
                lineas.loc[i,'Activa'] = linea.find('i').next_sibling

            self.datos_generales['Lineas de investigación'] = lineas
        else:
            if not self.response:
                print('Falta lectura del cvlac de {}'.format(self.name))
            else:
                print('No se encuentran lineas de investigación del investigador')
            self.datos_generales['Lineas de investigación'] = None
        return

#%% -------------- APROPIACIÓN SOCIAL  ---------------------------------------

    def eventos(self):

        from datetime import datetime
        import pandas as pd

        if self.response and 'Eventos científicos' in self.secciones:

            eventos_cient = self.secciones['Eventos científicos'].find_all('table')
            print('Agregando Apropiación social - Eventos científicos')

            produccion = pd.DataFrame(columns=['avalado','Nombre del evento','Tipo',
                                               'Ambito','Fecha inicio','Fecha fin',
                                               'Lugar','Instituciones asociadas',
                                               'Tipo de vinculación',
                                               'participantes',
                                               'Rol en el evento',
                                               'productos asociados',
                                               'Nombre producto','Tipo de producto'])

            for i, evento_c in enumerate(eventos_cient):

                if evento_c.find('img') and 'chulo' in evento_c.find('img')['src']:
                    produccion.loc[i,'avalado'] = True
                else: produccion.loc[i,'avalado'] = False

                produccion.loc[i,'Nombre del evento'] = (evento_c.find('i',
                                                         text='Nombre del evento:\xa0').
                                                   next_sibling.replace('\xa0\n','').strip())
                produccion.loc[i,'Tipo'] = (evento_c.find('i',text='Tipo de evento: ').
                                      next_sibling.replace('\xa0\n','').strip())
                produccion.loc[i,'Ambito'] = (evento_c.find('i',text='Ámbito: ').
                                        next_sibling.replace('\xa0','').
                                        replace('\n','').strip())

                fecha_inicio = (evento_c.find_all('i')[3].text.
                                split('Realizado el:')[1].split(',')[0])
                produccion.loc[i,'Fecha inicio'] = ((datetime.
                                                     strptime(fecha_inicio, '%Y-%m-%d %H:%M:%S.%f')).
                                                    date() if fecha_inicio else None)
                fecha_fin = evento_c.find_all('i')[3].text.split('\xa0\n')[1].strip()
                produccion.loc[i,'Fecha fin'] = ((datetime.
                                                  strptime(fecha_fin, '%Y-%m-%d %H:%M:%S.%f')).
                                                 date() if fecha_fin else None)

                instituciones = [institut.next_sibling.replace('\n','').strip()
                                 for institut in evento_c.
                                 find_all('i',text='Nombre de la institución:')]
                produccion.loc[i,'Instituciones asociadas'] = ';'.join(instituciones)

                vinculaciones = [s.next_sibling for s in evento_c.
                                 find_all('i',text='Tipo de vinculación')]
                produccion.loc[i,'Tipo de vinculación'] = ';'.join(vinculaciones)

                participantes = [tag.next_sibling.strip().strip('\n')
                                 for tag in evento_c.find_all('i',text='Nombre: ')]
                produccion['participantes'] = ';'.join(participantes)
                roles = [tag.next_sibling.strip().strip('\n') for tag in
                         evento_c.find_all('i',text='Rol en el evento: ')]
                produccion['Rol en el evento'] = ';'.join(roles)
                if evento_c.find('b',text='Productos asociados'):
                    produccion.loc[i,'productos asociados'] = True
                    productos = [p.next_sibling.strip().strip('\n') for p in
                                 evento_c.find_all('i',text='Nombre del producto:')]
                    produccion.loc[i,'Nombre producto'] = ';'.join(productos)
                    tipo_prod = [p.next_sibling.strip().strip('\n') for p in
                                 evento_c.find_all('i',text='Tipo de producto:')]
                    produccion['Tipo de producto'] = ';'.join(tipo_prod)
                else:
                    produccion.loc[i,'productos asociados'] = False
            self.apropiacion_social['Eventos cientificos'] = produccion
        else:
            print('No se han leido los eventos científicos del CVLAC de {}'.format(self.name))
            self.apropiacion_social['Eventos cientificos'] = None
        return

#%% --------------- PRODUCCIÓN BIBLIOGRÁFICA ----------------------------------
    def Articulos(self):

        import pandas as pd

        if self.response and 'Artículos' in self.secciones:
            print('Agregando produccion bibliográfica - Articulos')
            samples = self.secciones['Artículos'].find_all('td')[1:]

            qualifications = [c.find('li') for i, c in enumerate(samples) if i % 2 == 0]
            articles = [c.find('blockquote') for i,c in enumerate(samples) if i % 2 != 0]

            produccion = pd.DataFrame(columns=['avalado','Tipo','Autores','Titulo',
                                               'Pais','Nombre Revista','ISSN',
                                               'Editorial','Volumen','Fasículo',
                                               'Paginas','Editorial','Año',
                                               'DOI','palabras','sectores'])

            for calificacion, articulo, ind in zip(qualifications, articles, range(len(articles))):

                produccion.loc[ind,'avalado'] = ('si' if calificacion.find('img') 
                                                 and calificacion.find('img')['src'] else 'No')

                tipo = calificacion.find('b').text.split('-')[-2:]
                produccion.loc[ind,'Tipo'] = ''.join(tipo).strip(',').strip()

                Authors = articulo.text.split('"')[0]
                Aut_list = [autor.strip() for autor in Authors.split('\n')]
                produccion.loc[ind,'Autores'] = ' '.join(Aut_list).strip()

                produccion.loc[ind,'Titulo'] = articulo.text.split('"')[1]
                produccion.loc[ind,'Pais'] = articulo.text.split('En:')[1].split('\xa0')[0].strip()
                produccion.loc[ind,'Nombre Revista'] = articulo.find('br').next_sibling.strip()

                produccion.loc[ind,'ISSN'] = articulo.find('i',text='ISSN:').next_sibling.split('\xa0')[1]
                produccion.loc[ind,'Editorial'] = articulo.find('i',text='ed:').next_sibling
                volumen = articulo.find('i',text='v.').next_sibling.split('\n')[0]
                produccion.loc[ind,'Volumen'] = int(volumen) if volumen.isdigit() else None

                fields = articulo.find('i',text='fasc.').next_sibling.split('\n')
                produccion.loc[ind,'Fasículo'] = fields[0]
                p_inicial = int(fields[1].split('p.')[1]) if fields[1].split('p.')[1] != '' else None
                p_final = int(fields[2].split(' - ')[1]) if fields[2].split(' - ')[1] != '' else None
                produccion.loc[ind,'Paginas'] = tuple([p_inicial,p_final])
                produccion.loc[ind,'Año'] = int(fields[3].split(',')[1])
                produccion.loc[ind,'DOI'] = articulo.find('i',text='\xa0DOI:\xa0').next_sibling.split('\n')[0]

                #recopilando palabras claves y sectores
                if articulo.find('b',text='Palabras: ') and articulo.find('b',text='Sectores: '):
                    key_words = articulo.text.split('Palabras: ')[1].split('Sectores: ')[0]
                    palabras = ' '.join([p.strip() for p in key_words.split('\n')])
                    produccion.loc[ind,'palabras'] = palabras
                elif articulo.find('b',text='Palabras: '):
                    key_words = articulo.text.split('Palabras: ')[1]
                    palabras = ' '.join([p.strip() for p in key_words.split('\n')])
                    produccion.loc[ind,'palabras'] = palabras
                if articulo.find('b',text='Sectores: '):
                    sectores = articulo.text.split('Sectores: ')[1]
                    sectores = ''.join([s.strip() for s in sectores.split('\n')])
                    produccion.loc[ind,'sectores'] = sectores

            self.produccion_bibliografica['Artículo'] = produccion
        else:
            if not self.response:
                print('Falta lectura del cvlac de {}'.format(self.name))
            else:
                print('No se han leido resgisgros de artículos del investigador')
            self.produccion_bibliografica['Artículo'] = None
        return
    
    def libros_investigacion(self):
        import pandas as pd
    
        if self.response and 'Libros' in self.secciones:
    
            samples = self.secciones['Libros'].find_all('tr')[1:]
            print('Agregando produccion bibliográfica - Libros')
            produccion = pd.DataFrame(columns=['avalado','Tipo','Autores',
                                               'Titulo','Pais','año',
                                               'Editorial','ISBN','volumen',
                                               'páginas','palabras','Áreas'])
            qualification = [b.find('li') for i,b in enumerate(samples) if i % 2 ==0]
            books = [b.find('blockquote') for i,b in enumerate(samples) if i % 2 != 0]
    
    
            for calif, libro, i in zip(qualification,books,range(len(books))):
                # De calif obtenemos la avalación por colciencias y el tipo de producción
                if calif.find('img') and 'chulo' in calif.find('img')['src']:
                    produccion.loc[i,'avalado'] = 'Si'
                else:
                    produccion.loc[i,'avalado'] = 'No'
    
                produccion.loc[i,'Tipo'] = calif.find('b').text
    
                # Información de citación del libro
                autores = libro.text.split('"')[0].split('\n')
                produccion.loc[i,'Autores'] = ''.join(autor.strip() for autor in autores)[:-1]
                produccion.loc[i,'Titulo'] = libro.text.split('"')[1]
                produccion.loc[i,'Pais'] = libro.text.split('En:')[1].split('\n')[0].strip()
    
                year = libro.text.split('En:')[1].split('\n')[1].split('.\xa0')[0].strip()
                produccion.loc[i,'año'] = int(year) if year.isdigit() else None
    
                produccion.loc[i,'Editorial'] = libro.text.split('ed:')[1].split('\n')[0].strip()
                produccion.loc[i,'ISBN'] = libro.find('i',text='ISBN:').next_sibling.split('\xa0')[1]
    
                vol = libro.find('i',text='v. ').next_sibling.split('\n')[0]
                produccion.loc[i,'volumen'] = int(vol) if vol.isdigit() else None
                
                pags = libro.find('i',text='pags.').next_sibling.split('\n')[0].split('\xa0')[1]
                produccion.loc[i,'páginas'] = int(pags) if pags.isdigit() else None
                #recopilando palabras claves y Áreas de conocimiento
                if libro.find('b',text='Palabras: ') and libro.find('b',text='Areas: '):
                    key_words = libro.text.split('Palabras: ')[1].split('Areas: ')[0]
                    palabras = ' '.join([p.strip() for p in key_words.split('\n')])
                    produccion.loc[i,'palabras'] = palabras
                elif libro.find('b',text='Palabras: '):
                    key_words = libro.text.split('Palabras: ')[1]
                    palabras = ' '.join([p.strip() for p in key_words.split('\n')])
                    produccion.loc[i,'palabras'] = palabras
                if libro.find('b',text='Areas: '):
                    sectores = libro.text.split('Areas: ')[1]
                    sectores = ''.join([s.strip() for s in sectores.split('\n')]).replace(' --',',')
                    produccion.loc[i,'Áreas'] = sectores
                    
            self.produccion_bibliografica['Libro'] = produccion
        else:
            if not self.response:
                print('Falta lectura del cvlac de {}'.format(self.name))
            else:
                print('No se encontraron libros del investigador')
            self.produccion_bibliografica['Libro'] = None
    
        return
    
    def capitulos_de_libro(self):
        import pandas as pd
        if self.response and 'Capitulos de libro' in self.secciones:
            print('Agregando produccion bibliográfica - Capítulos de Libro')
            capitulos_libro = self.secciones['Capitulos de libro'].find_all('blockquote')
    
            produccion = pd.DataFrame(columns=['avalado','Tipo','Autores',
                                               'Titulo capítulo','Titulo de libro',
                                               'Pais','ISBN','editorial',
                                               'Volumen','Pagina','año','Palabras',
                                              'Áreas','Sectores'])
    
            for i, capitulo in enumerate(capitulos_libro):
    
                if capitulo.find('img') and 'chulo' in capitulo.find('img')['src']:
                    produccion.loc[i,'avalado'] = 'Si'
                else:
                    produccion.loc[i,'avalado'] = 'No'
    
                produccion.loc[i,'Tipo'] = capitulo.text.split('Tipo: ')[1].split('\n')[0]
                Autores = [c.strip() for c in capitulo.text.split('"')[0].split('\n')
                           if 'Tipo: ' not in c]
                produccion.loc[i,'Autores'] = ''.join(Autores)
                produccion.loc[i,'Titulo capítulo'] = capitulo.text.split('"')[1]
                produccion.loc[i,'Titulo de libro'] = (capitulo.text.split('"')[2].
                                                       split('\n')[1].strip())
                produccion.loc[i,'Pais'] = (capitulo.text.split('En:')[1].
                                            split('\xa0')[0].strip())
                produccion.loc[i,'ISBN'] = (capitulo.find('i',text='ISBN:').next_sibling.
                                            split('\n')[0].strip('\xa0'))
                produccion.loc[i,'editorial'] = (capitulo.find('i',text='ed:').next_sibling.
                                                split('\n')[0].strip('\xa0'))
                volumen = (capitulo.find('i',text=', v.').next_sibling.
                           split('\n')[1].split(',')[0].strip())
                produccion.loc[i,'Volumen'] = int(volumen) if volumen.isdigit() else None
                pag_inicio = (capitulo.find('i',text=', v.').
                              next_sibling.split('\n')[1].split('p.')[1])
                pag_final = (capitulo.find('i',text=', v.').
                             next_sibling.split('\n')[2].split('- ')[1].strip('\xa0'))
                if pag_inicio.isdigit() and pag_final.isdigit():
                    produccion.loc[i,'Pagina'] = (int(pag_inicio),int(pag_final))
                else:
                    produccion.loc[i,'Pagina'] = None
    
                year =  (capitulo.find('i',text=', v.').
                         next_sibling.split('\n')[4].split(',')[1])
                
                produccion.loc[i,'año'] = int(year) if year.isdigit() else None
    
                # Encontrando palabras claves, áreas y sectores
                fields = [b.text for b in capitulo.find_all('b')]
                if fields:
                    if len(fields) == 3: # existen los campos palabras, Areas y Sectores
                        keywords = (capitulo.text.split('Palabras: ')[1]
                                    .split('Areas: ')[0].split('\n'))
                        produccion.loc[i,'Palabras'] = ''.join([s.strip() 
                                                                for s in keywords])
                        areas = (capitulo.text.split('Areas: ')[1].
                                 split('Sectores: ')[0].split('\n'))
                        produccion.loc[i,'Áreas'] = (''.join([a.strip() for a in areas]).
                                                     replace(' --',''))
                        sectores = [s.strip() for s in 
                                    capitulo.text.split('Sectores: ')[1].split('\n')]
                        produccion.loc[i,'Sectores'] =''.join(sectores)
    
                    elif len(fields) == 2 and 'Palabras: ' in fields:
                        if 'Areas: ' in fields:
                            keywords = (capitulo.text.split('Palabras: ')[1].
                                        split('Areas: ')[0].split('\n'))
                            produccion.loc[i,'Palabras'] = (''.join([
                                                    s.strip() for s in keywords]))
                            areas = capitulo.text.split('Areas: ')[1].split('\n')
                            produccion.loc[i,'Áreas'] = (''.join([a.strip() 
                                                      for a in areas]).replace(' --',''))
                            produccion.loc[i,'Sectores'] = None
                        else:
                            keywords = (capitulo.text.split('Palabras: ')[1].
                                        split('Sectores: ')[0].split('\n'))
                            produccion.loc[i,'Palabras'] = (''.
                                                            join([s.strip() 
                                                              for s in keywords]))
                            sectores = [s.strip() for s in 
                                        capitulo.text.split('Sectores: ')[1].split('\n')]
                            produccion.loc[i,'Sectores'] =''.join(sectores)
                            produccion.loc[i,'Áreas'] = None
    
                    elif len(fields) == 2 and 'Palabras: ' not in fields:
                        areas = (capitulo.text.split('Areas: ')[1].
                                 split('Sectores: ')[0].split('\n'))
                        produccion.loc[i,'Áreas'] = (''.join([a.strip() for a in areas])
                                                     .replace(' --',''))
                        sectores = [s.strip() for s in 
                                    capitulo.text.split('Sectores: ')[1].split('\n')]
                        produccion.loc[i,'Sectores'] =''.join(sectores)
                        produccion.loc[i,'Palabras'] = None
                    else:
                        if capitulo.find('b',text='Palabras: '):
                            keywords = capitulo.text.split('Palabras: ')[1].split('\n')
                            produccion.loc[i,'Palabras'] = ''.join([s.strip() 
                                                                    for s in keywords])
                            produccion.loc[i,'Áreas'] = None
                            produccion.loc[i,'Sectores'] = None
                        elif capitulo.find('b',text='Areas: '):
                            areas = capitulo.text.split('Areas: ')[1].split('\n')
                            produccion.loc[i,'Áreas'] = (''.join([a.strip() 
                                                                  for a in areas]).
                                                         replace(' --',''))
                            produccion.loc[i,'Sectores'] = None
                            produccion.loc[i,'Palabras'] = None
                        else:
                            sectores = [s.strip() for s in 
                                        capitulo.text.split('Sectores: ')[1].split('\n')]
                            produccion.loc[i,'Sectores'] =''.join(sectores)
                            produccion.loc[i,'Palabras'] = None
                            produccion.loc[i,'Áreas'] = None
                else:
                    produccion.loc[i,'Palabras'] = None
                    produccion.loc[i,'Áreas'] = None
                    produccion.loc[i,'Sectores'] = None
            self.produccion_bibliografica['Capítulo de libro'] = produccion
        else:
            if not self.response:
                print('Falta lectura del cvlac de {}'.format(self.name))
            else:
                print('El investigador no posee capitulos de libro')
            self.produccion_bibliografica['Capítulo de libro'] = None
        return 

#%% --------------- FUNCIONES GENERALES --------------------------------------
def historial_posicion(trabajo):
    """Tomar una experiencia laboral llamada 'trabajo' del tipo bs4.element.Tag
    y genera un diccionatio con toda la información relacionada"""

    import pandas as pd
    Experiencia =dict.fromkeys(['Institucion','Dedicación','Métrica temporal',
                                'Fecha inicio','Fecha fin',
                                'Actividades de administración','Act_admin',
                                'Actividades de investigación','Act_invest',
                                'Actividades de docencia','Act_docen'],None)

    Experiencia['Institucion'] = trabajo.find('b').text
    horas = (trabajo.find('i',text='Dedicación: ').
             next_sibling.strip().split('\xa0')[0]).strip()
    Experiencia['Dedicación'] = int(horas) if horas.isdigit() else 0
    Experiencia['Métrica temporal'] = (trabajo.find('i',text='Dedicación: ').
                                       next_sibling.split('\xa0')[1].strip().split('\n')[0])
    Experiencia['Fecha inicio'] = fechas(trabajo.find('i',text='Dedicación: ').
                                         next_sibling.split('\n')[1].strip())
    Experiencia['Fecha fin'] = fechas(trabajo.find('i',text='Dedicación: ').
                                      next_sibling.split('\n')[2].strip())

    # Encontrando las modalidades de trabajo dentro del cargo realizado
    if trabajo.find('span',attrs={'class':'blueTitle'}):
        modalidades = [s.text for s in trabajo.find_all('span')]

    # Si existe la modalidad administrativa
        if 'Actividades de administración' in modalidades:
            Experiencia['Actividades de administración'] = True
            Act_admin = (trabajo.find('i',text=' - Cargo:').
                         next_sibling.strip().replace('\n',';').replace('  ',''))
            Experiencia['Act_admin'] = Act_admin.split(';')
            Experiencia['Act_admin'][1] = fechas(Experiencia['Act_admin'][1])
            if len(Experiencia['Act_admin']) == 3:
                Experiencia['Act_admin'][2] = fechas(Experiencia['Act_admin'][2])
            else:
                Experiencia['Act_admin'].append(fechas('de'))
        else:
            Experiencia['Actividades de administración'] = False

    # si existe la modalidad de investigación
        if 'Actividades de investigación' in modalidades:
            Experiencia['Actividades de investigación'] = True
            Experiencia['Act_invest'] = {}
            modelo = (trabajo.text.split(' - Titulo:')[0].
                      split('Actividades de investigación')[1].split('\n')[-2].strip())
            Experiencia['Act_invest']['modalidad invest'] = modelo

            Act_investigacion = (trabajo.find('i',text=' - Titulo:').
                                 next_sibling.strip().replace('  ','').split('\n'))
            Experiencia['Act_invest']['Titulo'] = Act_investigacion[0]
            Experiencia['Act_invest']['Fecha inicio'] = fechas(Act_investigacion[1])
            if len(Act_investigacion) == 3:
                Experiencia['Act_invest']['Fecha fin'] = fechas(Act_investigacion[2])
            else:
                Experiencia['Act_invest']['Fecha fin'] = (fechas('de'))

        else:
            Experiencia['Actividades de investigación'] = False

    # si existe la modalidad de docencia
        if 'Actividades de docencia' in modalidades:
            Experiencia['Actividades de docencia'] = True
            act_docencia = trabajo.text.split('Actividades de docencia')[1].split('\n')
            cursos = [space.replace('\xa0',' ') for space in act_docencia]
            ind = [i for i, e in enumerate(cursos) if 'Nombre del curso:' in e]
            historial_curos = []

            for i, e in enumerate(ind):
                asignatura = pd.DataFrame(columns=(['Nivel educativo','Asignatura',
                                                    'Fecha inicio','Fecha fin']))

                asignatura.loc[0,'Nivel educativo'] =  cursos[e-1].strip()
                asignatura.loc[0,'Asignatura'] = (cursos[e].split(':')[1].
                                                  split(',')[0].strip())

                try:
                    asignatura.loc[0,'Fecha inicio'] = fechas(cursos[e+1].strip())
                except:
                    asignatura.loc[0,'Fecha inicio'] = None
                try:
                    asignatura.loc[0,'Fecha fin'] = fechas(cursos[e+2].strip())
                except:
                    asignatura.loc[0,'Fecha fin'] = None

                historial_curos.append(asignatura)

            Experiencia['Act_docen'] = pd.concat(historial_curos,ignore_index=True)
        else:
            Experiencia['Actividades de docencia'] = False

    return Experiencia

def fechas(fecha_in_string):
    """Entrega una fecha en formato datetime
    de una string tipo <mes>de<año>
    INT
    fecha_in_string: str: entrada tipo fecha,
        ejm: Septiembrede2012, datetime(2012,9,1,0,0,0)
        ejm: Septiembre 2012, datetime(2012,9,1,0,0,0)
        ejm: Septiembrede 2012, datetime(2012,9,1,0,0,0)
        ejm: de 2012, datetime(2012,1,1,0,0,0)
        ejm: Actual, datetime.now()
        ejm: Enero de, None
        ejm: de, None
    OUT
    date datetime: fecha y hora en formato datetime
    """
    from datetime import datetime
    import re

    p = re.compile('^\w+\s+[0-9]{4}$') # coincidencia para 'Mes Año' ej. 'Marzo 2020'
    pyear = re.compile('^de+\s+[0-9]{4}') # solo el año ej. 'de 2005'
    pyear2 =re.compile('^de+[0-9]{4}') # solo año ej. 'de2005'
    meses = {'Enero':1,'Febrero':2,'Marzo':3,'Abril':4,'Mayo':5,'Junio':6,
             'Julio':7,'Agosto':8,'Septiembre':9,
             'Octubre':10,'Noviembre':11,'Diciembre':12}

    if 'Actual' in fecha_in_string:
        anho = int(datetime.now().year)
        mes = int(datetime.now().month)
        dia = int(datetime.now().day)
        date = datetime(year=anho, month=mes, day=dia)
    elif fecha_in_string.strip() == 'de':
        date = None
    elif pyear2.match(fecha_in_string):
        anho = int(fecha_in_string.split('de')[1])
        date = datetime(year=anho,month=1, day = 1)
    elif p.match(fecha_in_string):
        if pyear.match(fecha_in_string):
            anho = int(fecha_in_string.split(' ')[1])
            date = datetime(year=anho,month=1, day = 1)
        else:
            fecha_in_string = fecha_in_string.replace('de','')
            anho = int(fecha_in_string.split(' ')[1])
        try:
            mes = meses[fecha_in_string.split(' ')[0]]
        except:
            mes = meses['Enero']

        date = datetime(year=anho,month=mes, day=1)
    else:
        try: # si existe el mes colocarlo
            mes = meses[fecha_in_string.split('de')[0].strip()]
        except: # si no, colocar el mes de Enero por defecto
            mes = meses['Enero'] # si el mes no esta por defecto es mes 1
        try: # si exsite el año colocar el año y el mes  como fecha tipo datetime
            anho = int(fecha_in_string.strip()[-4:])
            date = datetime(year=anho, month=mes, day=1)
        except: # si no existe el año se colocará la fecha como Nonetype,
                # para generar una futura alarma
            date = None

    return date

def Curso_profesional(nivel):
    '''Programa para convertir un elemnto bs4.tag en un pandas DataFrame
    para su posterior análisis

    nivel: bst.tag: toda la información del nivel a convertir
    '''
    import pandas as pd

    estudio = pd.DataFrame(columns=['Nivel académico','Institución','Titulo',
                          'Fecha Inicio','Fecha Fin','Profundización'])

    estudio.loc[0,'Nivel académico'] = nivel.find('b').text
    datos = nivel.text.split('\n')[1:]
    estudio.loc[0,'Institución'] = datos[0].replace('  ','')
    estudio.loc[0,'Titulo'] = datos[1].replace('  ','') # Titulo otorgado
    estudio.loc[0,'Fecha Inicio'] = fechas(datos[2].replace('  ','').split(' - ')[0])
    estudio.loc[0,'Fecha Fin'] = fechas(datos[2].replace('  ','').split(' - ')[1])
    estudio.loc[0,'Profundización'] = ('\n'.join(datos[3:])
                                 .replace('  ','').replace('\r',' '))

    return estudio

def Curso_complementario(curso_pequenho):
    """
    Convierte un elemento bs4.tag en un pandas DataFrame con toda la información pertinente
    curso_pequenho: bs4.tag: Tag con la información del curso complementario

    Parameters
    ----------
    curso_pequenho : TYPE bs4.tag
        DESCRIPTION. toda la información a extraer de un curso pequeño

    Returns
    -------
    curso_complementario : pandas.DataFrame
        DESCRIPTION. La misma información pero ordenada y estructura en un DataFrame

    """
    import pandas as pd

    curso_complementario = pd.DataFrame(columns=['Tipo curso','Institución','Titulo',
                                                 'Fecha inicio','Fecha fin'])

    curso_complementario.loc[0,'Tipo curso'] = curso_pequenho.find('b').text
    datos_curso = curso_pequenho.text.split('\n')[1:]
    curso_complementario.loc[0,'Institución'] = datos_curso[0].replace('  ','')
    curso_complementario.loc[0,'Titulo'] = datos_curso[1].replace('  ','')
    curso_complementario.loc[0,'Fecha inicio'] = fechas(datos_curso[2].
                                                         replace('  ','').
                                                         split(' - ')[0])
    curso_complementario.loc[0,'Fecha fin'] = fechas(datos_curso[2].
                                                     replace('  ','').
                                                     split(' - ')[1])

    return curso_complementario

#%% ************** CLASE GRUPO DE INVESTIGACION ******************************
class Grupo():

    def __init__(self):
        self.name = None
        self.gruplac_url = None
        self.response = 0
        self.GroupLac_html = None
        self.investigadores = {} # objetos de la clase investigador
        self.secciones = {}

        # propios del Grouplac
        self.datos_basicos = None
        self.instituciones = None
        self.plan_estrategico = None
        self.lineas_de_investigacion_declaradas_por_el_grupo = None
        self.integrantes_del_grupo = None
        self.produccion_de_formacion_y_extension = None
        self.programa_academico_de_doctorado = None
        self.programa_academico_de_maestria = None
        self.otro_programa_academico = None
        self.curso_de_doctorado = None
        self.curso_de_maestria = None
        self.curso_especializado_de_extension = None
        self.produccion_bibliografica = None
        self.articulos_publicados = None
        self.libros_publicados = None
        self.capitulos_de_libro_publicados = None
        self.documentos_de_trabajo_ = None
        self.otra_publicacion_divulgativa = None
        self.otros_articulos_publicados = None
        self.otros_libros_publicados = None
        self.traducciones_ = None
        self.notas_cientificas = None
        self.produccion_tecnica_y_tecnologica = None
        self.cartas_mapas_o_similares = None
        self.conceptos_tecnicos = None
        self.diseños_industriales = None
        self.esquemas_de_trazados_de_circuito_integrado = None
        self.informes_tecnicos = None
        self.innovaciones_en_procesos_y_procedimientos = None
        self.innovaciones_generadas_en_la_gestion_empresarial = None
        self.nuevas_variedades_animal = None
        self.poblaciones_mejoradas_de_razas_pecuarias = None
        self.nuevas_variedades_vegetal = None
        self.nuevos_registros_cientificos = None
        self.plantas_piloto = None
        self.productos_nutraceuticos = None
        self.otros_productos_tecnologicos = None
        self.prototipos = None
        self.regulaciones_y_normas = None
        self.protocolos_de_vigilancia_epidemiologica = None
        self.reglamentos_tecnicos = None
        self.guias_de_practica_clinica = None
        self.proyectos_de_ley = None
        self.signos_distintivos_ = None
        self.softwares_ = None
        self.empresas_de_base_tecnologica_ = None
        self.apropiacion_social_y_circulacion_del_conocimiento = None
        self.consultorias_cientifico_tecnologicas = None
        self.ediciones = None
        self.eventos_cientificos = None
        self.informes_de_investigacion = None
        self.nuevas_secuencias_geneticas = None
        self.redes_de_conocimiento_especializado = None
        self.generaciones_de_contenido_de_audio = None
        self.generacion_de_contenido_impreso = None
        self.generacion_de_contenido_multimedia = None
        self.generacion_de_contenido_virtual = None
        self.estrategias_de_comunicacion_del_conocimiento = None
        self.estrategias_pedagogicas_para_el_fomento_a_la_cti = None
        self.espacios_de_participacion_ciudadana = None
        self.participacion_ciudadana_en_proyectos_de_cti = None
        self.produccion_en_arte_arquitectura_y_diseño = None
        self.actividades_de_formacion = None
        self.asesorias_al_programa_ondas = None
        self.curso_de_corta_duracion_dictados = None
        self.trabajos_dirigidos_turorias = None
        self.actividades_como_evaluador = None
        self.jurado_comisiones_evaluadoras_de_trabajo_de_grado = None
        self.participacion_en_comites_de_evaluacion = None
        self.demas_trabajos = None
        self.proyectos = None

    def add_investigador(self,ID_investigador, investigador):
        """
        Methodos para agregar al grupo un miembro de la clase Investigador

        Parameters
        ----------
        ID_investigador : Int
            DESCRIPTION. Indentificación del Investigador
        investigador : class Investigador
            DESCRIPTION. Toda la información del investigador en la estructura
                        class Investigador
        Returns
        -------
        None.

        """
        print('Agregando informacion de {} al Grupo'.format(investigador.name))
        self.investigadores[ID_investigador] = investigador

    def lecturaGroupLac(self, gruplac_url):
        """ Metodo para leer el GroupLac del grupo de investigación y guardarlo
        en una soup de Beautifulsoup

        gruplac_url: param str: direccción web del GroupLac
        """
        import requests, bs4, time
        from datetime import datetime
        # genrados por control
        self.gruplac_url = gruplac_url


        try:
            response = requests.get(url= gruplac_url, verify=False)
            GroupLac_soup = bs4.BeautifulSoup(response.text,'lxml')
            self.name = (GroupLac_soup.
                         find('span', attrs={'class':"celdaEncabezado"}).text)
            print('Leyendo GroupLac de {} \n:'.format(self.name))
            self.GroupLac_html = GroupLac_soup
            self.response = response.status_code
            self.fecha_GroupLac = datetime.today() # última fecha de web scrapping
            # todas las tablas del gruplac
            tablas = (GroupLac_soup.find('body').
                      find_all('table',
                           attrs={'width':'100%','style':'border:#999 1px solid;'}))

            # obteniendo un diccionario con todas las secciones tipo tabla del cvlac
            secciones = [tabla.find('td', attrs={'class':'celdaEncabezado'}).
                         text.strip().replace(' ','_') for tabla in tablas
                         if tabla.find('td', attrs={'class':'celdaEncabezado'})]

            print('Obteniendo las secciones del GroupLac {} \n'.format(self.name))
            self.secciones = (dict.fromkeys(secciones, None))

            for tabla in tablas: # loop por toda la informción de las tablas
                for section in self.secciones.keys(): #loop por cada sección
                # si existe un titulo en esta tabla
                    if tabla.find('td', attrs={'class':'celdaEncabezado'}):
                        if (section == tabla.
                            find('td', attrs={'class':'celdaEncabezado'}).
                            text.strip().replace(' ','_')):
                            # print('agregando {}'.format(section))
                            self.secciones[section] = tabla
            time.sleep(8)
        except Exception as e:
            print('No se pudo leer cvlac de {}, error {}'.
                  format(self.name ,e))
            
    def lista_integrantes(self):
        
        import pandas as pd
        from datetime import datetime
    
        if 'Integrantes_del_grupo' in self.secciones:
    
            members_tab = pd.DataFrame(columns=['Nombre','Vinculación','Horas dedicación',
                                                      'fecha inicio','fecha fin','cvlac'])
    
            integrantes = self.secciones['Integrantes_del_grupo'].find_all('tr')[2:]
    
            for i, member in enumerate(integrantes):
    
                members_tab.loc[i, 'Nombre'] = member.find('a').text
                members_tab.loc[i,'Vinculación'] =\
                    member.find_all('td')[1].text.split('\n')[1].strip()
                horas = member.find_all('td')[2].text.split('\n')[1].strip()
                members_tab.loc[i,'Horas dedicación'] = int(horas)
                tiempo = member.find_all('td')[3].text.split('\n')[1].strip()
                members_tab.loc[i,'fecha inicio'] = (datetime.strptime(tiempo.
                                                   split(' -')[0], '%Y/%m')).date()
                
                if 'Actual' in tiempo.split('-')[1]:
                    members_tab.loc[i,'fecha fin'] = 'Actual'
                else:
                    members_tab.loc[i,'fecha fin'] = (datetime.strptime(tiempo.
                                                    split('- ')[1], '%Y/%m')).date()
    
                members_tab.loc[i,'cvlac'] = member.find('a')['href']
            
            self.integrantes_del_grupo = members_tab
        else:
            print('No se encontraron integrantes del gurpo dentro den gruplac')
            self.integrantes_del_grupo = None
        
        return members_tab

    def basicos(self):
        """Funcion para sustraer datos básicos del gruplac de un grupo de investigación"""
        from datetime import datetime
        import pandas as pd

        if 'Datos_básicos' in self.secciones:

            campos_info = self.secciones['Datos_básicos'].find_all('tr')
            print('Agregando datos básicos del grupo {} \n'.format(self.name))
            basics = {}

            # Ordenando primera información en diccionario
            datos_basicos = pd.DataFrame(
                columns=(['Fecha creación','Departamento - Ciudad','Líder',
                          'Dia de certificación','Pagina web',
                          'E-mail','Clasificación','Área de conocimiento',
                          'Programa nacional de ciencia y tecnología',
                          'Programa nacional de ciencia y tecnología (secundario)']))

            for campo in campos_info[1:]:

                basics[campo.find('td',attrs={'class':'celdasTitulo'}).text] = \
                    campo.find('td',attrs={'class':'celdas2'}).text

            # Limpiando información y guardandola en dataframe

            datos_basicos.loc[1,'Fecha creación'] = datetime.strptime(
                basics['Año y mes de formación'], '%Y - %m' )

            datos_basicos.loc[1,'Departamento - Ciudad'] = basics['Departamento - Ciudad']
            datos_basicos.loc[1,'Líder'] = basics['Líder']

            if 'Si' in basics['¿La información de este grupo se ha certificado? ']:
                date = datetime.strptime(
                    basics['¿La información de este grupo se ha certificado? '].
                    strip('\n').strip().split(' ')[-1],'%Y-%m-%d')

                datos_basicos.loc[1,'Dia de certificación'] = date
            else:
                datos_basicos.loc[1,'Dia de certificación'] = None

            datos_basicos.loc[1,'Pagina web'] = campos_info[5].find('a', href=True)['href']
            datos_basicos.loc[1,'E-mail'] = campos_info[6].find('a', href=True)['href']

            datos_basicos.loc[1,'Clasificación'] = basics['Clasificación'].split('\n')[1]
            datos_basicos.loc[1,'Área de conocimiento'] = (basics['Área de conocimiento'].
                                                     split('\n')[1].strip())

            datos_basicos.loc[1,'Programa nacional de ciencia y tecnología'] = \
                basics['Programa nacional de ciencia y tecnología']
            datos_basicos.loc[1,'Programa nacional de ciencia y tecnología (secundario)']= \
                basics['Programa nacional de ciencia y tecnología (secundario)'].strip()

            self.datos_basicos = datos_basicos
        else:

            print('Los datos básicos de {} no estan disponibles \n'.format(self.name))
            self.datos_basicos = None

        return

    def articulos(self):
        """ Función para enumerar los artículos del grupo"""

        import pandas as pd

        if len(self.secciones['Artículos_publicados'].find_all('tr')) > 1:

            print('Agregando los artículos del grupo')
            articles = [article.find('td',attrs={'class':'celdas1'})
                        for article in self.secciones['Artículos_publicados'].
                        find_all('tr')]
            articles = self.secciones['Artículos_publicados'].find_all('tr')[1:]

            produccion = pd.DataFrame(
                columns=['avalado','tipo','titulo','pais','revista','ISSN',
                         'año','DOI','autores','publindex','scimagojr'])
            for i, article in enumerate(articles):

                if article.find('img') and 'chulo' in article.find('img')['src']:
                    produccion.loc[i,'avalado'] = 'Si'
                else: produccion.loc[i,'avalado'] = 'No'

                fields = article.find_all('td')
                produccion.loc[i,'tipo'] = fields[1].find('strong').text.strip(':')
                produccion.loc[i,'titulo'] = fields[1].find('strong').next_sibling
                produccion.loc[i,'pais'] = (fields[1].text.split('\n')[2].
                                            split(',')[0].strip())
                produccion.loc[i,'revista'] = (fields[1].text.split('\n')[2].
                                               split(',')[1].split('ISSN')[0].strip())
                produccion.loc[i,'ISSN'] = (fields[1].text.split('ISSN:')[1].
                                            split(',')[0].strip())
                produccion.loc[i,'año'] = int(fields[1].text.split('vol:')[0].
                                              split(',')[-1])
                produccion.loc[i,'DOI'] = (fields[1].find_all('strong')[1].
                                           next_sibling.strip('\n').strip())
                produccion.loc[i,'autores'] = (fields[1].text.split('\n')[-2].
                                               split('Autores:')[1][:-2])
                produccion.loc[i,'publindex'] = None
                produccion.loc[i,'scimagojr'] = None

            self.articulos_publicados = produccion
        else:
            print('Aun no se han descargado los artículos publicados del grupo {} \n'.
                  format(self.name))
            self.articulos_publicados = None

    def libros(self):
        """
        Función para enumerar los libros publicados del grupo

        Returns
        -------
        None.

        """
        import pandas as pd

        if len(self.secciones['Libros_publicados'].find_all('tr')) > 1 :

            print('Agregando los libros publicados del grupo')
            libros = self.secciones['Libros_publicados'].find_all('tr')[1:]

            produccion = pd.DataFrame(columns=['avalado','tipo','titulo',
                                               'pais','año','ISBN','volumen',
                                               'pags','Editorial','autores'])

            for i, libro in enumerate(libros):
                if libro.find('img') and 'chulo' in libro.find('img')['src']:
                    produccion.loc[i,'avalado'] = 'Si'
                else: produccion.loc[i,'avalado'] = 'No'

                fields = libro.find_all('td')
                produccion.loc[i,'tipo'] = (fields[1].find('strong').
                                            text.strip(':'))
                produccion.loc[i,'titulo'] = (fields[1].find('strong').
                                              next_sibling.strip()[1:])
                produccion.loc[i,'pais'] = (fields[1].text.split('\n')[2].
                                            split(',')[0].strip())
                produccion.loc[i,'año'] = int(fields[1].text.split('\n')[2].
                                           split(',')[1])
                produccion.loc[i,'ISBN'] = (fields[1].text.split('ISBN:')[1].
                                            split('vol')[0].strip())
                produccion.loc[i,'volumen'] = (fields[1].text.split('vol:')[1].
                                               split('págs')[0].strip())
                produccion.loc[i,'pags'] = (fields[1].text.
                                            split('págs:')[1].
                                            split(',')[0].strip())
                produccion.loc[i,'Editorial'] = (fields[1].text.split('\n')[2].
                                                 split('Ed. ')[1].strip())
                produccion.loc[i,'autores'] = (fields[1].text.split('\n')[-2].
                                               split('Autores:')[1][:-2])

            self.libros_publicados = produccion
        else:
            print('Aun no se han descargado los libros del grupo {} \n'.
              format(self.name))
            self.libros_publicados = None

        return

    def capitulos_libro(self):

        import pandas as pd

        if len(self.secciones['Capítulos_de_libro_publicados'].find_all('tr')) > 1:

            print('Agregando los capítulos de libro al grupo')
            capitulos = (self.secciones['Capítulos_de_libro_publicados'].
                         find_all('tr')[1:])

            produccion = pd.DataFrame(columns=['avalado','tipo capitulo',
                                               'titulo capitulo','pais','año',
                                      'titulo libro','ISBN','volumen','pags',
                                      'Editorial','autores'])

            for i, capitulo in enumerate(capitulos):
                if capitulo.find('img') and 'chulo' in capitulo.find('img')['src']:
                    produccion.loc[i,'avalado'] = 'Si'
                else: produccion.loc[i,'avalado'] = 'No'
                fields = capitulo.find_all('td')

                produccion.loc[i,'tipo'] = (fields[1].find('strong').
                                            text.strip(':'))
                produccion.loc[i,'titulo capitulo'] = (fields[1].find('strong').
                                                       next_sibling.strip()[2:])
                produccion.loc[i,'pais'] = fields[1].text.split('\n')[2].split(',')[0].strip()
                produccion.loc[i,'año'] = int(fields[1].text.split('\n')[2].split(',')[1])
                produccion.loc[i,'titulo libro'] = (fields[1].text.split('\n')[2].
                                                    split(', ISBN')[0])
                produccion.loc[i,'ISBN'] = (fields[1].text.split('ISBN:')[1].
                                            split(',')[0].strip())
                produccion.loc[i,'volumen'] = (fields[1].text.split('Vol.')[1].
                                               split(',')[0].strip())
                produccion.loc[i,'pags'] = (fields[1].text.split('págs:')[1].
                                            split(',')[0].strip())
                produccion.loc[i,'Editorial'] = (fields[1].text.split('\n')[2].
                                                 split('Ed. ')[1].strip())
                produccion.loc[i,'autores'] = (fields[1].text.split('\n')[-2].
                                               split('Autores:')[1][:-2].strip())

            self.capitulos_de_libro_publicados = produccion
        else:
            print('Aun no hay información de los capítulos de libros del grupo {} \n'.
                  format(self.name))
            self.capitulos_de_libro_publicados = None

        return

    def eventos(self):

        import pandas as pd
        from datetime import datetime

        if len(self.secciones['Eventos_Científicos'].find_all('tr')) > 1:

            print('Agregando los eventos del grupo')
            eventos_cientificos = (self.secciones['Eventos_Científicos'].
                                   find_all('tr')[1:])

            produccion = pd.DataFrame(columns=['avalado','tipo','titulo',
                                           'ciudad','fecha inicio',
                                           'fecha fin','ambito',
                                           'Tipo de participación',
                                        'Instituciones asociadas',
                                        'Tipo de vinculación'])

            for i, evento in enumerate(eventos_cientificos):

                if evento.find('img') and 'chulo' in evento.find('img')['src']:
                    produccion.loc[i,'avalado'] = 'Si'
                else: produccion.loc[i,'avalado'] = 'No'

                fields = evento.find_all('td')
                produccion.loc[i,'tipo'] = fields[1].find('strong').text
                produccion.loc[i,'titulo'] = fields[1].find('strong').next_sibling[2:]
                produccion.loc[i,'ciudad'] = (fields[1].text.split('\n')[2].
                                              split(',')[0].strip())
                fecha_inicio = (fields[1].text.split('\n')[2].
                                split('desde')[1][:-2].strip())
                produccion.loc[i,'fecha inicio'] = ((datetime.strptime(
                                                fecha_inicio, '%Y-%m-%d')).date()
                                                    if fecha_inicio else None)
                fecha_fin = fields[1].text.split('\n')[3].split('hasta')[1].strip()
                produccion.loc[i,'fecha fin'] = ((datetime.strptime(
                                                fecha_fin, '%Y-%m-%d')).date()
                                                 if fecha_fin else None)
                produccion.loc[i,'ambito'] = (fields[1].text.split('\n')[4].
                                              split('Ámbito:')[1].split(',')[0].strip())
                produccion.loc[i,'Tipo de participación'] = (fields[1].text.split('\n')[4].
                                                             split('Tipos de participación:')[1].
                                                             strip())

                institutuciones = [tag.next_sibling.replace('\n,','').strip().title()
                                   for tag in fields[1].
                                   find_all('i',text='Nombre de la institución:')]
                produccion.loc[i,'Instituciones asociadas'] = ', '.join(institutuciones)

                tipo_vinculacion = ([tag.next_sibling for 
                                     tag in fields[1].
                                     find_all('i',text='Tipo de vinculación')])

                produccion.loc[i,'Tipo de vinculación'] = ', '.join(tipo_vinculacion)

                self.eventos_cientificos = produccion
        else:
            print('No se registraron eventos científicos del grupo')
            self.eventos_cientificos = None
        return
    
    def proyectos_investigacion(self):
    
        import pandas as pd
        from datetime import date
    
        if len(self.secciones['Proyectos'].find_all('tr')) > 1:
            print('Agregando el historial de proyectos de investigación')
            proyectos = self.secciones['Proyectos'].find_all('tr')[1:]
    
            produccion = pd.DataFrame(columns=['avalado','tipo',
                                               'nombre','fecha inicio',
                                               'fecha fin',])
    
            #separando proyecto por proyecto en una lista de tuples
            data_proyectos = [(p.find_all('td')) for p in proyectos] 
    
            for i, proyecto in enumerate(data_proyectos):
                if proyecto[0].find('img'):
                    produccion.loc[i,'avalado'] = 'Si'
                else:
                    produccion.loc[i,'avalado'] = 'No'
    
                produccion.loc[i,'tipo'] = proyecto[1].find('b').text
                produccion.loc[i,'nombre'] = proyecto[1].find('b').next_sibling[2:]
                # recoleccion de año y mes de inicio del proyecto
                try:
                    anho_inicio = proyecto[1].text.split('\n')[3].split('/')[0].strip()
                    mes_inicio = proyecto[1].text.split('\n')[3].split('/')[1][:2].strip()
                except:
                    # si no se pueden sustraer la informacion mandarla a string
                    anho_inicio = 'None'
                    mes_inicio = 'None'
                # si los datos sustraidos no son string pero numericos ejecutar
                if anho_inicio.isdigit() and mes_inicio.isdigit():
                    produccion.loc[i,'fecha inicio'] = date(year=int(anho_inicio),
                                                            month=int(mes_inicio),day=1)
                else:
                    produccion.loc[i,'fecha inicio'] = None
                    
                # recoleccion de año y mes de inicio del proyecto
                try:
                    fecha_fin = proyecto[1].text.split('\n')[4].strip()
                    if fecha_fin.split('/')[0].isdigit() and fecha_fin.split('/')[1].isdigit():
                        produccion.loc[i,'fecha fin'] = date(year=int(fecha_fin.split('/')[0]),
                                                             month=int(fecha_fin.split('/')[1]),
                                                            day=1)
                    elif 'actual' in fecha_fin.lower():
                        produccion.loc[i,'fecha fin'] = date.today()
                    else:
                        produccion.loc[i,'fecha fin'] = fecha_fin
                except:
                    produccion.loc[i,'fecha fin'] = None
            
            self.proyectos = produccion
        else:
            print('No se registraron eventos científicos del grupo')
            self.proyectos = None
            
        return